import string as st
import pymorphy2
from copy import deepcopy, copy
from math import sqrt
import pandas as pd


def lcs(s1, s2):
    # longest common substring of two strings s1 and s2
    m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
    longest, x_longest = 0, 0
    for x in range(1, 1 + len(s1)):
        for y in range(1, 1 + len(s2)):
            if s1[x - 1] == s2[y - 1]:
                m[x][y] = m[x - 1][y - 1] + 1
                if m[x][y] > longest:
                    longest = m[x][y]
                    x_longest = x
            else:
                m[x][y] = 0
    return s1[x_longest - longest: x_longest]


def numMatch(n1, n2):
    # returns number from 0 to 1 (1 if numbers are similar)
    if (n1 != 0) and (n2 != 0):
        return 1 - abs(n1 - n2) / (n1 + n2)
    else:
        return 0


def strMatch(s1, s2):
    # returns number from 0 to 1 (1 if strings are similar)
    lc = len(lcs(s1, s2))
    # print(s1, ' ', s2, ' ', lc)
    l1 = len(s1)
    l2 = len(s2)
    return (lc + lc) / (l1 + l2)


class Bigram:
    # class of bigram
    words = ['', '']    # pair of words
    rate = 0.0          # average rate
    var = 0.0           # variation of rate
    num = 0             # count of includes
    con = 1             # concentrate: (N - P)/(N + P + 1) where N - count of negative rates, P - count of positive rates
    pos = 0             # count of positive (with rate <3) comments
    neg = 0             # count of negative (with rate >3) comments
    wc = 0              # average inverse distance between words
    maxDist = 3         # maximal distance between words (if distance is more wc = 0).

    def __init__(self, word1, word2, rate, dist=1):
        self.words = [word1, word2]
        self.wc = self.WC(dist, self.maxDist)
        self.num = 1
        self.rate = rate
        self.var = 0
        self.con = 1

    def push(self, rate, dist=1):
        old_rate = self.rate
        self.rate = (old_rate * self.num + rate) / (self.num + 1)
        self.wc = (self.wc * self.num + self.WC(dist, self.maxDist)) / (self.num + 1)
        self.var = (self.var * self.num + rate * rate + old_rate * old_rate * self.num -
                    (self.num + 1) * self.rate * self.rate
                    ) / (self.num + 1)
        if rate > 3:
            self.pos += 1
        elif rate < 3:
            self.neg += 1
        self.con = abs(self.pos - self.neg) / self.num
        self.num += 1
        pass

    def __repr__(self):
        return str([self.words, self.rate, self.var, self.wc])

    def reverse(self):
        # returns the same bigram with reversed pair of words
        revObj = deepcopy(self)
        revObj.words.reverse()
        return revObj

    def compare(self, other):
        # returns True if two bigrams are similar (considering order)
        k1 = strMatch(''.join(self.words), ''.join(other.words))
        k2 = numMatch(self.rate, other.rate)
        k3 = numMatch(self.var, other.var)
        p = (1 * k1 + 0 * k2 + 0 * k3)
        # print(k1, ' ', k2, ' ', k3, ' ', p)
        # print(p)
        return p > 0.5

    def areEquiv(self, other):
        # returns True if two bigrams are equivalent
        return (self.words == other.words) or (self.reverse().words == other.words)

    def __eq__(self, other):
        # returns True if two bigrams are similar
        return self.compare(other) \
               or self.compare(other.reverse()) \
               or self.reverse().compare(other) \
               or self.reverse().compare(other.reverse())

    def __contains__(self, item):
        # returns True if string item is similar to bigram string
        return (strMatch(''.join(self.words), item) < 0.3) or (strMatch(''.join(self.reverse().words), item) < 0.3)

    @staticmethod
    def WC(dist, maxDist):
        if 0 < dist < maxDist:
            return 1 - (dist - 1) / (maxDist - 1)
        else:
            return 0


class BigramEqClass:
    # class of bigrams equivalence class
    bigrams = []    # list of bigrams
    impact = 0      # impact factor of whole class
    var = 0         # average variation of bigrams
    con = 0         # average concentration of bigrams
    rate = 0        # average rate
    num = 0         # count of pushes

    error1 = 'Error: object must belong to Bigram class'    # error string

    def __init__(self, obj):
        if not isinstance(obj, Bigram):
            print(self.error1)
        else:
            self.bigrams = []
            self.impact = 0
            self.bigrams.append(obj)

    def __add__(self, obj):
        if not isinstance(obj, Bigram):
            print(self.error1)
        else:
            found = False
            for i in range(0, len(self.bigrams)):
                if self.bigrams[i].areEquiv(obj):
                    found = True
                    self.bigrams[i].push(obj.rate, obj.wc)
                    break
            if not found:
                self.bigrams.append(obj)

    def calcImpact(self):
        # calculate class impact factor
        for i in range(0, len(self.bigrams)):
            self.var += self.bigrams[i].var
            self.con += self.bigrams[i].con
            self.num += self.bigrams[i].num
            self.rate += self.bigrams[i].rate
        self.var /= len(self.bigrams)
        self.con /= len(self.bigrams)
        self.rate /= len(self.bigrams)
        self.impact = self.con * self.num / (1 + self.var)

    def isMatch(self, obj):
        if not isinstance(obj, Bigram):
            print(self.error1)
        else:
            counter = 0
            for i in range(0, len(self.bigrams)):
                if obj == self.bigrams[i]:
                    counter += 1
                elif self.bigrams[i].areEquiv(obj):
                    counter += len(self.bigrams)
            return counter > 0.9 * len(self.bigrams)

    def push(self, obj):
        if not isinstance(obj, Bigram):
            print(self.error1)
            return -1
        elif self.isMatch(obj):
            self + obj
            return 0
        else:
            return 1

    def __contains__(self, item):
        # returns True if class contains bigrams contains the item
        result = 0
        for i in range(0, len(self.bigrams)):
            if item in self.bigrams[i]:
                result += 1
        return result > 0

    def __repr__(self):
        self.calcImpact()
        result = '\nimpact factor: ' + repr(self.impact) + '\naverage rate: ' + repr(self.rate) + \
                 '\ncon = ' + repr(self.con) + '; var = ' + repr(self.var) + '; num = ' + repr(self.num) + '\n'\
                 'list of bigrams:\n'
        for i in range(0, len(self.bigrams)):
            result += repr(i) + ': ' + repr(self.bigrams[i].words) + '; rate = ' + repr(self.bigrams[i].rate) +\
                      '; var = ' + repr(self.bigrams[i].var) + '; num = ' + repr(self.bigrams[i].num) + \
                      '; con = ' + repr(self.bigrams[i].con) + '\n'
        return result

    def __len__(self):
        return len(self.bigrams)

    def __del__(self):
        self.bigrams = []
        self.impact = 0
        return self


class BigramDict:
    classes = []

    error1 = 'Error: object must belong to BigramEqClass class'

    def __init__(self, obj, printReport=0):
        if isinstance(obj, BigramEqClass):
            self.classes.append(obj)
        if isinstance(obj, pd.DataFrame):
            listOfBigrams = []
            for i in range(0, len(obj.text)):
                words = obj.text[i].replace('.', '').replace(',', '').replace('"', '').replace('  ', ' ').split(' ')
                for j in range(0, len(words)-1):
                    listOfBigrams.append(Bigram(words[j], words[j+1], obj.rate[i], 1))
                for j in range(0, len(words) - 2):
                    listOfBigrams.append(Bigram(words[j], words[j + 2], obj.rate[i], 2))
            itemsToIgnore = []
            for i in range(0, len(listOfBigrams) - 1):
                if i not in itemsToIgnore:
                    auxBigramClass = BigramEqClass(listOfBigrams[i])
                    for j in range(i + 1, len(listOfBigrams)):
                        if j not in itemsToIgnore:
                            if not auxBigramClass.push(listOfBigrams[j]):
                                itemsToIgnore.append(j)
                                if printReport:
                                    print(repr(i) + ': ' + repr(listOfBigrams[i]) + ' | ' + repr(j) + ': '
                                    + repr(listOfBigrams[j]))
                    if len(auxBigramClass) > 1:
                        self.classes.append(deepcopy(auxBigramClass))

    def __repr__(self):
        result = ''
        for i in range(0, len(self.classes)):
            result += '\n' + repr(i) + ' ' + repr(self.classes[i].bigrams[0]) + '\n' \
                      + repr(self.classes[i].bigrams[1]) + '...\n'
            result += '     count of elements: ' + repr(len(self.classes[i]))
        return result

    def __len__(self):
        return len(self.classes)

myList = [('я нашла там ужасно красивые двери', 4.0),
          ('хорошо что двери которые я нашла были красивые просто ужасно', 5.0),
          ('плохо что двери которые я нашла были просто ужасны', 1.0),
          ('хорошо что я нашла там ужасно красивые двери', 4.0),
          ('я не нашла там ужасно красивые двери', 1.0),
          ('я не нашла там ужасно красивые двери', 1.0),
          ('плохо что я не нашла там ужасно красивые двери', 1.0),
          ('я нашла там ужасно красивые двери', 4.0),
          ('я нашла там ужасные двери', 1.0),
          ('я нашла там ужасные двери', 1.0),
          ('я нашла там ужасно красивые двери', 4.0)]

pan = pd.DataFrame(myList, columns=['text', 'rate'])
lib = BigramDict(pan)
for i in range(0, (len(lib))):
    print(lib.classes[i])


